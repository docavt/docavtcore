import { TodoActions } from 'app/actions/todos';
import * as React from 'react';
import { TodoTextInput } from '../TodoTextInput';

export namespace Header {
  export interface Props {
    addTodo: typeof TodoActions.addTodo;
  }
}

export class Header extends React.Component<Header.Props> {
  constructor(props: Header.Props, context?: any) {
    super(props, context);
    this.handleSave = this.handleSave.bind(this);
  }

  handleSave(text: string) {
    if (text.length) {
      this.props.addTodo({ text });
    }
  }

  render() {
    return (
      <header>
        <h1>Todos</h1>
        <TodoTextInput newTodo={true} onSave={this.handleSave} placeholder="What needs to be done?" />
      </header>
    );
  }
}
